import Promise from 'bluebird';

class AuthApi {

  registerDevice(deviceId) {
    if (deviceId) {
      const options = {
        method: 'POST',
        body: {
          deviceId
        }
      };
      return fetch('172.16.10.131:8080/registerDevice', options).then((apiResponse) => {
        return apiResponse.json();
      });
    }
    return {};
  }

}

export default new AuthApi();
