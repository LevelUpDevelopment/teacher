import AuthApi from '../services/AuthApi.js';
import { Actions } from 'react-native-router-flux';
import storeFactory from '../storeFactory.js';

const registerDevice = () => {
  return {
    type: 'REGISTER_DEVICE'
  };
};

const AuthActions = {

  registerDevice: (deviceId) => {
    return dispatch => {
      return AuthApi.registerDevice(deviceId).then(result => {
        if (result.response) {
          return dispatch(registerDevice(result.response));
        }
        // TODO: handle failure.
      }).catch(() => { // eslint-disable-line handle-callback-err
        // TODO: handle failure.
      });
    };
  }

};

export default AuthActions;
