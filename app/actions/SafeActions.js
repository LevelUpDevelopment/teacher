import SafeApi from '../services/SafeApi.js';

const setSafe = () => {
  return {
    type: 'SET_SAFE'
  };
};

const SafeActions = {

  setSafe: (isSafe) => {
    return dispatch => {
      return SafeApi.setSafe(isSafe).then((result) => {
        if (result.response) {
          return dispatch(setSafe(result.response));
        }
        return null;
        // TODO: Error if response failed
      }).catch(() => { // eslint-disable-line handle-callback-err
        // TODO: Error if response failed
        return null;
      });
    };
  }

};

export default SafeActions;
