function AuthReducer(state = {}, action) {
  switch (action.type) {
  case 'TRY_LOGIN':
    return Object.assign({}, state, {
      loginInProgress: true,
      validationError: false
    });
  case 'RECEIVE_LOGIN':
    return Object.assign({}, state, {
      token: action.token,
      user: action.user,
      loginInProgress: false,
      validationError: false
    });
  case 'LOGIN_FAILED':
    return Object.assign({}, state, {
      loginInProgress: false,
      validationError: true
    });
  case 'LOGOUT':
    return {};
  default:
    return state;
  }
}

export default AuthReducer;
