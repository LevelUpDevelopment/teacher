import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import AuthReducer from './reducers/AuthReducer.js';

this.store = null;

export default {

  configureStore: function (preloadedState) { // eslint-disable-line object-shorthand
    this.store = createStore(
      combineReducers({
        Auth: AuthReducer
      }),
      preloadedState,
      applyMiddleware(
        thunkMiddleware
      )
    );
    return this.store;
  },
  store: this.store
};
