import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Text, TextInput, TouchableOpacity, View, Picker, ImageBackground } from 'react-native';

import AuthActions from '../actions/AuthActions.js';
import SafeActions from '../actions/SafeActions.js';

import DeviceInfo from 'react-native-device-info';

import { Actions } from 'react-native-router-flux';

import {
  MKButton,
  MKColor
} from 'react-native-material-kit';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class Safe extends Component {

  constructor(props, context) {
    super(props, context);
    this.props = props;
    this.context = context;
    this.appStyles = context.appStyles;

    this.componentStyles = StyleSheet.create({

    });

    this.rooms = [
      {
        id: 'asdf',
        roomName: 'test'
      },
      {
        id: 'asdf2',
        roomName: 'test2'
      },
      {
        id: 'asdf3',
        roomName: 'test3'
      }
    ];

    this.state = {
      room: null,
      selectedRoom: null,
      isSafe: null
    };
  }

  componentWillMount() {
  }

  componentDidMount() {
    AuthActions.registerDevice('asdf');
  }

  setSafe(isSafe) {
    this.setState({ isSafe }, () => {
      SafeActions.setSafe(isSafe);
    });
  }

  setSelectedRoom() {
    if (!this.state.selectedRoom && this.state.room !== null) {
      return this.setState({ selectedRoom: this.rooms[this.state.room] });
    }
    return this.setState({ selectedRoom: null });
  }

  render() {
    return (
      <KeyboardAwareScrollView>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginBottom: 10, alignItems: 'center' }}>
            <Text style={{ color: '#000000', padding: 20, fontSize: 25 }}>Are you Safe and Secure?</Text>
          </View>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
            <MKButton
              style={{ flex: 1, justifyContent: 'center', borderWidth: 2, borderColor: '#262626', alignItems: 'center', paddingVertical: 20, marginHorizontal: 20 }}
              backgroundColor={(this.state.isSafe !== null && this.state.isSafe) ? '#6fdc6f' : '#f2f2f2'}
              shadowRadius={2}
              shadowOffset={{ width: 0, height: 2 }}
              shadowOpacity={0.7}
              shadowColor="black"
              onPress={this.setSafe.bind(this, true)}
            >
              <Text pointerEvents="none" style={{
                color: (this.state.isSafe !== null && this.state.isSafe) ? '#FFFFFF' : '#262626',
                fontWeight: 'bold',
                fontSize: 20
              }}
              >
                YES
              </Text>
            </MKButton>
            <MKButton
              style={{ flex: 1, borderWidth: 2, borderColor: '#262626', justifyContent: 'center', alignItems: 'center', paddingVertical: 20, marginHorizontal: 20 }}
              backgroundColor={(this.state.isSafe !== null && !this.state.isSafe) ? '#ff704d' : '#f2f2f2'}
              shadowRadius={2}
              shadowOffset={{ width: 0, height: 2 }}
              shadowOpacity={0.7}
              shadowColor="black"
              onPress={this.setSafe.bind(this, false)}
            >
              <Text pointerEvents="none" style={{
                color: (this.state.isSafe !== null && !this.state.isSafe) ? '#FFFFFF' : '#262626',
                fontWeight: 'bold',
                fontSize: 20
              }}
              >
                NO
              </Text>
            </MKButton>
          </View>
          <View>
            <Text style={{ color: '#000000', padding: 20, fontSize: 25 }}>Please choose your room (or nearest room to you):</Text>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 1 }}>
              {!this.state.selectedRoom &&
                <View>
                  <Picker
                    selectedValue={this.state.room}
                    onValueChange={(rid) => {
                      this.setState({ room: rid });
                    }}
                    mode="dropdown"
                  >
                    <Picker.Item key="room_select" label="Select Your Room" value={null} />
                    {this.rooms.map((r, rid) => {
                      return (
                        <Picker.Item key={`room_${rid}`} label={r.roomName} value={rid} />
                      );
                    })}
                  </Picker>
                </View>
              }
              <View>
                <MKButton
                  style={{ flex: 0, borderWidth: 2, borderColor: '#262626', justifyContent: 'center', alignItems: 'center', paddingVertical: 20, marginHorizontal: 20 }}
                  backgroundColor="#000099"
                  shadowRadius={2}
                  shadowOffset={{ width: 0, height: 2 }}
                  shadowOpacity={0.7}
                  shadowColor="black"
                  activeOpacity={this.state.selectedRoom ? 0.3 : 1}
                  onPress={this.setSelectedRoom.bind(this)}
                >
                  {this.state.selectedRoom &&
                    <Text pointerEvents="none" style={{
                      color: '#FFFFFF',
                      fontWeight: 'bold',
                      fontSize: 20
                    }}
                    >
                      {this.state.selectedRoom.roomName}
                    </Text>
                  }
                  {!this.state.selectedRoom &&
                    <Text pointerEvents="none" style={{
                      color: '#FFFFFF',
                      fontWeight: 'bold',
                      fontSize: 20
                    }}
                    >
                      Select Room
                    </Text>
                  }
                </MKButton>
              </View>
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }

}

Safe.defaultProps = {
  user: null,
  token: '',
  loginInProgress: false,
  validationError: false
};

Safe.propTypes = {
  dispatch: PropTypes.func.isRequired,
  user: PropTypes.object,
  token: PropTypes.string,
  loginInProgress: PropTypes.bool,
  validationError: PropTypes.bool,
  syncGatewaySession: PropTypes.object
};

function mapReduxStoreStateToProps(reduxStoreState) {
  return {
    token: reduxStoreState.Auth.token,
    user: reduxStoreState.Auth.user,
    loginInProgress: reduxStoreState.Auth.loginInProgress,
    validationError: reduxStoreState.Auth.validationError,
    syncGatewaySession: reduxStoreState.Auth.syncGatewaySession
  };
}

export default connect(mapReduxStoreStateToProps)(Safe);
