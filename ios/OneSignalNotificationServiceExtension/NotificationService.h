//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by lalcaraz on 4/20/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
