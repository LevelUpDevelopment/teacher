import PropTypes from 'prop-types';
import React, {
  Component
} from 'react';

import {
  Provider,
  connect
} from 'react-redux';

import {
  View
} from 'react-native';

import {
  Scene,
  Router,
  Reducer,
  NavigationActions
} from 'react-native-router-flux';

import _ from 'lodash';

// route components
import Safe from './app/components/Safe.js';
// actions / stores
import AuthActions from './app/actions/AuthActions.js';

import storeFactory from './app/storeFactory.js';
const store = storeFactory.configureStore();

import OneSignal from 'react-native-onesignal';

console.disableYellowBox = true; // eslint-disable-line no-console

class Teacher extends Component {

  constructor(props, context) {
    super(props, context);
    this.props = props;
    this.context = context;

    this.hasLoaded = false;

    this.routerReducerCreate = this.routerReducerCreate.bind(this);

  }

  componentWillMount() {
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  componentDidMount() {
    this.hasLoaded = true;
  }

  shouldComponentUpdate() {
    if (!this.hasLoaded) {
      return true;
    }
    return false;
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds(device) {
      console.log('Device info: ', device);
  }

  routerReducerCreate(params) {
    const defaultReducer = new Reducer(params);
    return (state, action) => {
      if (action.type === 'REACT_NATIVE_ROUTER_FLUX_RESET') {
        NavigationActions.resetStack.defer(action.key);
      } else if (action.type === 'REACT_NATIVE_ROUTER_FLUX_REPLACE') {
        NavigationActions.replaceRoute.defer(action.key);
      } else if (action.scene) {
        NavigationActions.setCurrentScene.defer(action.scene.name);
      }
      return defaultReducer(state, action);
    };
  }

  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene
            key="Safe"
            type="reset"
            component={Safe}
            initial
          />
        </Scene>
      </Router>
    );
  }
}

Teacher.childContextTypes = {};

Teacher.defaultProps = {
  dispatch: null,
  settings: null,
  store: null,
  user: null
};

Teacher.propTypes = {
  dispatch: PropTypes.function,
  user: PropTypes.object
};

function mapReduxStoreStateToProps(reduxStoreState) {
  return {
    user: _.get(reduxStoreState.Auth, 'user', {})
  };
}

Teacher = connect(mapReduxStoreStateToProps)(Teacher); // eslint-disable-line no-class-assign

// Need to wrap main component in new Root component for Provider, so that props can be received from store.
const RootProviderWrapperComponent = () => ( // eslint-disable-line react/no-multi-comp
  <Provider store={store}>
    <Teacher />
  </Provider>
);

export default RootProviderWrapperComponent;
